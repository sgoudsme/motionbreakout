EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:RJ45 J2
U 1 1 5D502A79
P 1600 4200
F 0 "J2" H 1655 4867 50  0000 C CNN
F 1 "RJ45" H 1655 4776 50  0000 C CNN
F 2 "Connector_RJ:RJ45_Amphenol_54602-x08_Horizontal" V 1600 4225 50  0001 C CNN
F 3 "~" V 1600 4225 50  0001 C CNN
	1    1600 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector:RJ45 J1
U 1 1 5D502B39
P 1600 2950
F 0 "J1" H 1655 3617 50  0000 C CNN
F 1 "RJ45" H 1655 3526 50  0000 C CNN
F 2 "Connector_RJ:RJ45_Amphenol_54602-x08_Horizontal" V 1600 2975 50  0001 C CNN
F 3 "~" V 1600 2975 50  0001 C CNN
	1    1600 2950
	1    0    0    -1  
$EndComp
$Comp
L Sensor_Optical:LDR03 R1
U 1 1 5D502D0C
P 3500 4750
F 0 "R1" V 3175 4750 50  0000 C CNN
F 1 "LDR03" V 3266 4750 50  0000 C CNN
F 2 "OptoDevice:R_LDR_10x8.5mm_P7.6mm_Vertical" V 3675 4750 50  0001 C CNN
F 3 "http://www.elektronica-componenten.nl/WebRoot/StoreNL/Shops/61422969/54F1/BA0C/C664/31B9/2173/C0A8/2AB9/2AEF/LDR03IMP.pdf" H 3500 4700 50  0001 C CNN
	1    3500 4750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x05_Male J4
U 1 1 5D502FC0
P 4400 4250
F 0 "J4" H 4373 4180 50  0000 R CNN
F 1 "Conn_01x05_Male" H 4373 4271 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 4400 4250 50  0001 C CNN
F 3 "~" H 4400 4250 50  0001 C CNN
	1    4400 4250
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J3
U 1 1 5D5031E3
P 4300 3350
F 0 "J3" H 4380 3392 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 4380 3301 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-3_P5.08mm" H 4300 3350 50  0001 C CNN
F 3 "~" H 4300 3350 50  0001 C CNN
	1    4300 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5D50368C
P 2650 4500
F 0 "#PWR0101" H 2650 4250 50  0001 C CNN
F 1 "GND" H 2655 4327 50  0000 C CNN
F 2 "" H 2650 4500 50  0001 C CNN
F 3 "" H 2650 4500 50  0001 C CNN
	1    2650 4500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5D5037AA
P 2650 4200
F 0 "#PWR0102" H 2650 4050 50  0001 C CNN
F 1 "+5V" H 2665 4373 50  0000 C CNN
F 2 "" H 2650 4200 50  0001 C CNN
F 3 "" H 2650 4200 50  0001 C CNN
	1    2650 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4500 2650 4500
Wire Wire Line
	2000 4200 2650 4200
Text GLabel 2100 4300 2    50   Input ~ 0
motionOut
Wire Wire Line
	2000 4300 2100 4300
Text GLabel 3850 4250 0    50   Input ~ 0
motionOut
Wire Wire Line
	3850 4250 4200 4250
$Comp
L power:GND #PWR0103
U 1 1 5D503E02
P 4100 4350
F 0 "#PWR0103" H 4100 4100 50  0001 C CNN
F 1 "GND" H 4105 4177 50  0000 C CNN
F 2 "" H 4100 4350 50  0001 C CNN
F 3 "" H 4100 4350 50  0001 C CNN
	1    4100 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4350 4200 4350
$Comp
L power:+5V #PWR0104
U 1 1 5D503E43
P 4100 4150
F 0 "#PWR0104" H 4100 4000 50  0001 C CNN
F 1 "+5V" H 4115 4323 50  0000 C CNN
F 2 "" H 4100 4150 50  0001 C CNN
F 3 "" H 4100 4150 50  0001 C CNN
	1    4100 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4150 4200 4150
$Comp
L power:GND #PWR0105
U 1 1 5D503F18
P 3700 4800
F 0 "#PWR0105" H 3700 4550 50  0001 C CNN
F 1 "GND" H 3705 4627 50  0000 C CNN
F 2 "" H 3700 4800 50  0001 C CNN
F 3 "" H 3700 4800 50  0001 C CNN
	1    3700 4800
	1    0    0    -1  
$EndComp
Text GLabel 2100 4400 2    50   Input ~ 0
ldr
Wire Wire Line
	2100 4400 2000 4400
Text GLabel 3150 4750 0    50   Input ~ 0
ldr
Wire Wire Line
	3150 4750 3350 4750
Wire Wire Line
	3700 4800 3700 4750
Wire Wire Line
	3700 4750 3650 4750
Text GLabel 2100 4000 2    50   Input ~ 0
DmxPlus
Text GLabel 2100 3900 2    50   Input ~ 0
DmxMin
Text GLabel 2100 4100 2    50   Input ~ 0
DmxGnd
Wire Wire Line
	2000 3900 2100 3900
Wire Wire Line
	2000 4000 2100 4000
Wire Wire Line
	2000 4100 2100 4100
Wire Wire Line
	2000 3800 2800 3800
Wire Wire Line
	2800 3800 2800 2950
Wire Wire Line
	2800 2950 2000 2950
Text GLabel 2100 3050 2    50   Input ~ 0
DmxMin
Text GLabel 2100 3150 2    50   Input ~ 0
DmxPlus
Text GLabel 2100 3250 2    50   Input ~ 0
DmxGnd
Wire Wire Line
	2000 3250 2100 3250
Wire Wire Line
	2000 3150 2100 3150
Wire Wire Line
	2000 3050 2100 3050
Text GLabel 4000 3450 0    50   Input ~ 0
DmxGnd
Text GLabel 4000 3350 0    50   Input ~ 0
DmxMin
Text GLabel 4000 3250 0    50   Input ~ 0
DmxPlus
Wire Wire Line
	4000 3450 4100 3450
Wire Wire Line
	4000 3350 4100 3350
Wire Wire Line
	4000 3250 4100 3250
$Comp
L power:GND #PWR0106
U 1 1 5D505D9A
P 2550 2700
F 0 "#PWR0106" H 2550 2450 50  0001 C CNN
F 1 "GND" H 2555 2527 50  0000 C CNN
F 2 "" H 2550 2700 50  0001 C CNN
F 3 "" H 2550 2700 50  0001 C CNN
	1    2550 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2850 2250 2850
Wire Wire Line
	2250 2850 2250 2750
Wire Wire Line
	2250 2550 2000 2550
Wire Wire Line
	2000 2650 2250 2650
Connection ~ 2250 2650
Wire Wire Line
	2250 2650 2250 2550
Wire Wire Line
	2000 2750 2250 2750
Connection ~ 2250 2750
Wire Wire Line
	2250 2750 2250 2700
Wire Wire Line
	2250 2700 2550 2700
Connection ~ 2250 2700
Wire Wire Line
	2250 2700 2250 2650
$EndSCHEMATC
